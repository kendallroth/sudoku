// Types
import { ICell, IRelatedCells } from "@typings";

/**
 * Check if cells are in the same block
 *
 * @param   target - Target cell
 * @param   cell   - Comparison cell
 * @returns Whether cells are in the same block
 */
const checkSameBlock = (target: ICell | null, cell: ICell | null): boolean => {
  if (!cell || !target) return false;

  /*return (
    Math.floor(cell.coordinates.x / 3) ===
      Math.floor(target.coordinates.x / 3) &&
    Math.floor(cell.coordinates.y / 3) === Math.floor(target.coordinates.y / 3)
  );*/

  return target.block.x === cell.block.x && target.block.y === cell.block.y;
};

/**
 * Check if cells are the same
 *
 * @param   target - Target cell
 * @param   cell   - Comparison cell
 * @returns Whether cells are the same
 */
const checkSameCell = (target: ICell | null, cell: ICell | null): boolean => {
  if (!cell || !target) return false;

  return checkSameBlock(target, cell) && checkSameColumn(target, cell);
};

/**
 * Check if cells are in the same column
 *
 * @param   target - Target cell
 * @param   cell   - Comparison cell
 * @returns Whether cells are in the same column
 */
const checkSameColumn = (target: ICell | null, cell: ICell | null): boolean => {
  if (!cell || !target) return false;

  return target.coordinates.x === cell.coordinates.x;
};

/**
 * Check if cells are in the same row
 *
 * @param   target - Target cell
 * @param   cell   - Comparison cell
 * @returns Whether cells are in the same row
 */
const checkSameRow = (target: ICell | null, cell: ICell | null): boolean => {
  if (!cell || !target) return false;

  return target.coordinates.y === cell.coordinates.y;
};

/**
 * Get all related cells to a target cell (row, column, block)
 *
 * NOTE: Will contain duplicates!
 *
 * @param   target      - Target cell
 * @param   grid        - Entire grid of cells
 * @param   includeSelf - Whether to include self in relations
 * @returns All related cells grouped by relation
 */
const getRelatedCells = (
  target: ICell,
  grid: ICell[][],
  includeSelf = false
): IRelatedCells => {
  const relatedCells: IRelatedCells = {
    block: [],
    column: [],
    row: [],
  };

  for (let y = 0; y < grid.length; y++) {
    for (let x = 0; x < grid[y].length; x++) {
      const cell = grid[y][x];
      if (!includeSelf && checkSameCell(target, cell)) continue;

      if (checkSameColumn(target, cell)) {
        relatedCells.row.push(cell);
      }
      if (checkSameRow(target, cell)) {
        relatedCells.column.push(cell);
      }
      if (checkSameBlock(target, cell)) {
        relatedCells.block.push(cell);
      }
    }
  }

  return relatedCells;
};

/**
 * Get all deduplicated related cells to a target cell (row, column, block)
 *
 * NOTE: Will not contain duplicates!
 *
 * @param   target - Target cell
 * @param   grid   - Entire grid of cells
 * @param   includeSelf - Whether to include self in relations
 * @returns All related cells (deduplicated)
 */
const getAllRelatedCells = (
  target: ICell,
  grid: ICell[][],
  includeSelf = false
): ICell[] => {
  const relatedCells = getRelatedCells(target, grid, includeSelf);

  const cellsWithDuplicates = [
    ...relatedCells.block,
    ...relatedCells.column,
    ...relatedCells.row,
  ];

  const deduplicatedCells = cellsWithDuplicates.reduce((accum, cell) => {
    return { ...accum, [cell.id]: cell };
  }, {});

  return Object.values(deduplicatedCells);
};

export {
  checkSameBlock,
  checkSameCell,
  checkSameColumn,
  checkSameRow,
  getRelatedCells,
  getAllRelatedCells,
};
