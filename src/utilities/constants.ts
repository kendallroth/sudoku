/** Number of cells in Sudoku grid */
export const CELLS_IN_GRID = 9;

/** Puzzle difficulty */
export enum PuzzleType {
  EASY = "easy",
  MEDIUM = "medium",
  HARD = "hard",
}
