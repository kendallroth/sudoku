// Utilities
import { CELLS_IN_GRID } from "./constants";

/**
 * Convert a Sudoku grid to a string
 *
 * @param   grid - Sudoku grid
 * @returns Sudoku string
 */
const gridToString = (grid: string[][]): string | null => {
  if (!grid || !grid.length) return null;

  return grid.map((r) => r.join("")).join("");
};

/**
 * Convert a Sudoku string to a grid
 *
 * @param   input  - Sudoku string
 * @param   length - Grid length (for splitting)
 * @returns Sudoku grid
 */
const stringToGrid = (
  input: string,
  length = CELLS_IN_GRID
): string[][] | null => {
  if (!input) return null;

  const rows = input.match(new RegExp(`.{1,${length}}`, "g"));
  if (!rows) return null;

  return rows.map((r) => r.split(""));
};

export { gridToString, stringToGrid };
