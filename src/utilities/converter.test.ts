// Utilities
import { gridToString, stringToGrid } from "./converter";

describe("Converts from Sudoku string to grid", () => {
  it("Handles empty input", () => {
    const input = "";

    const output = stringToGrid(input);

    expect(output).toEqual(null);
  });

  it("Handles valid input", () => {
    const input = "1.2.3.4.5";

    const output = stringToGrid(input, 3);

    const expected = [
      ["1", ".", "2"],
      [".", "3", "."],
      ["4", ".", "5"],
    ];

    expect(output).toEqual(expected);
  });
});

describe("Converts from Sudoku grid to string", () => {
  it("Handles empty input", () => {
    const input: string[][] = [];

    const output = gridToString(input);

    expect(output).toEqual(null);
  });

  it("Handles valid input", () => {
    const input = [
      ["1", ".", "2"],
      [".", "3", "."],
      ["4", ".", "5"],
    ];

    const output = gridToString(input);

    const expected = "1.2.3.4.5";

    expect(output).toEqual(expected);
  });
});
