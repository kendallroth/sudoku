// Utilities
import { stringToGrid } from "./converter";
import { CELLS_IN_GRID, PuzzleType } from "./constants";

// Types
import { ICell } from "../typings";

type IPuzzleSeeds = {
  [key in PuzzleType]: string[];
};

/** Puzzle seeds are represented by prefilled strings and grouped by difficulty */
export const PUZZLE_SEEDS: IPuzzleSeeds = {
  easy: [
    "3.5.2.1.9.8...6..527....4...3.6825.1.9.....4.5.8479.3...9....171..2...5.4.7.3.9.6",
  ],
  medium: [],
  hard: [],
};

// TODO: Write method to randomize/permutate puzzle based on existing seed

/**
 * Generate a Sudoku puzzle grid
 *
 * @param   type - Puzzle difficulty
 * @returns Sudoku puzzle grid
 */
const generateGrid = (type: PuzzleType): ICell[][] => {
  const typeSeeds = PUZZLE_SEEDS[type];
  const seedIdx = Math.floor(Math.random() * typeSeeds.length);
  const seed = PUZZLE_SEEDS[type][seedIdx];
  const stringGrid = stringToGrid(seed);
  if (!stringGrid) {
    throw new Error("Error reading Sudoku grid seed");
  }

  const tiles: ICell[][] = [];

  for (let y = 0; y < CELLS_IN_GRID; y++) {
    tiles[y] = [];

    for (let x = 0; x < CELLS_IN_GRID; x++) {
      const cellString: string = stringGrid[y][x];
      const number: number | null =
        cellString === "." ? null : parseInt(cellString, 10);

      const blockX = Math.floor(x / 3);
      const blockY = Math.floor(y / 3);

      tiles[y][x] = {
        block: { x: blockX, y: blockY },
        coordinates: { x, y },
        id: `${x},${y}`,
        notes: [],
        number,
        prefilled: Boolean(number),
      };
    }
  }

  return tiles;
};

export { generateGrid };
