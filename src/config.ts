// Utilities
import { version } from "../package.json";

/** App configuration */
export interface IConfig {
  /** Git repository url */
  gitUrl: string;
  /** App version */
  version: string;
}

/** App configuration */
const config: IConfig = {
  gitUrl: process.env.VUE_APP_GIT_URL,
  version: version,
};

export default config;
