export interface ICell {
  /** Parent block coordinates */
  block: ICoordinates;
  /** Cell coordinates */
  coordinates: ICoordinates;
  /** Cell ID (composed of coordinates) */
  id: string;
  /** Notes about possible numbers */
  notes: number[];
  /** Selected number */
  number: number | null;
  /** Whether cell is prefilled */
  prefilled: boolean;
}

/** Cell coordinates */
export interface ICoordinates {
  /** Column coordinate */
  x: number;
  /** Row coordinate */
  y: number;
}

/** Game options */
export interface IGameOptions {
  /** Whether to highlight similar numbers */
  highlightSameNumber: boolean;
  /** Whether to highlight numbers within the same block */
  highlightSelectedBlock: boolean;
  /** Whether to highlight numbers within the same row/column */
  highlightSelectedLines: boolean;
}

/** List of cells related to a target cell */
export interface IRelatedCells {
  /** Cells in the same block */
  block: ICell[];
  /** Cells in the same column */
  column: ICell[];
  /** Cells in the same row */
  row: ICell[];
}
