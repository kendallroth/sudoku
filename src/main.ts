import "@fontsource/mulish/variable.css";

import {
  create as createNaive,
  NButton,
  NCheckbox,
  NIcon,
  NModal,
} from "naive-ui";
import { createApp } from "vue";

// Styles
import "@styles/app.scss";

// Components
import App from "./App.vue";
import { SvgIcon } from "@components/layout";

// Utilities
import "./utilities/registerServiceWorker";
import store from "./store";

const app = createApp(App);

const naive = createNaive({
  components: [NButton, NCheckbox, NIcon, NModal],
});

app.use(store);
app.use(naive);

app.component("SvgIcon", SvgIcon);

app.mount("#app");
