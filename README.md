# sudoku

> Simple Sudoku game developed with Vue 3

## Inspiration

- [Sudoku Generator](https://javascript.plainenglish.io/building-a-sudoku-puzzle-generator-d55cead9656d)
- [Sudoku Game](https://medium.com/@sitianliu_57680/building-a-sudoku-game-in-react-ca663915712)

## Development

```sh
# Install dependencies
npm install

# Run in development (hot reloading)
npm run serve

# Build for production (minifies)
npm run build

# Run tests
npm run test

# Lint and fix files
npm run lint
```

> **NOTE:** Linting and formatting are run automatically upon commit.

## Icons

Generate icons with [`vue-pwa-asset-generator`](https://www.npmjs.com/package/vue-pwa-asset-generator)

```sh
npx vue-pwa-asset-generator -a ./src/assets/logo.png -o ./public/img/icons
```
