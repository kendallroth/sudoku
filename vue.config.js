// eslint-disable-next-line @typescript-eslint/no-var-requires
const path = require("path");

const src = path.resolve(__dirname, "src");

module.exports = {
  lintOnSave: false,

  configureWebpack: {
    resolve: {
      alias: {
        "@": src,
        "@assets": path.join(src, "assets"),
        "@components": path.join(src, "components"),
        "@config": path.join(src, "config.ts"),
        "@styles": path.join(src, "styles"),
        "@typings": path.join(src, "types"),
        "@utilities": path.join(src, "utilities"),
      },
    },
  },

  pwa: {
    name: "Sudoku",
  },
};
